import React, { Component } from 'react'
import api from '../../Apis/base'
import { withRouter } from 'react-router-dom'
import MenuBar from '../../Components/menuBar'

class LoginForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: ''
        }
    }

    handleChange = e => {
        const { value, name } = e.target
        this.setState({
            [name]: value
        })
    }

    loginSubmit = e => {
        e.preventDefault()
        api.post("/login", { username: this.state.username, password: this.state.password })
            .then((response) => {
                if (response.ok && response.status === 200) {
                    if (response.data.code === "0000") {
                        sessionStorage.setItem('response', response.data.results)
                        sessionStorage.setItem('isLogin', true)
                        this.props.history.push('/Search')
                    }
                    else {
                        alert(response.data.message)
                    }
                }
            })
    }
    render() {
        return (
            <div>
                <MenuBar />
                <div className="container">
                    <br />
                    <h2 className="text-left">Login..</h2>
                    <hr />
                    <center>
                        <div className="card col-5">
                            <div className="text-left form-row">
                                <div className="form-group col-md-12">
                                    <label className="col-form-label-lg">Username :</label>
                                    <div className="input-group flex-nowrap">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text"><i className="fas fa-user"></i></span>
                                        </div>
                                        <input type="text" name="username" className="form-control" onChange={this.handleChange} required />
                                    </div>
                                </div>
                            </div>

                            <div className="text-left form-row">
                                <div className="form-group col-md-12">
                                    <label className="col-form-label-lg">Password :</label>
                                    <div className="input-group flex-nowrap">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text"><i className="fas fa-lock"></i></span>
                                        </div>
                                        <input type="password" name="password" className="form-control" onChange={this.handleChange} required />
                                    </div>
                                </div>
                            </div>
                            <div className="text-center">
                                <button className="btn btn-light" onClick={this.loginSubmit}> Login </button>
                            </div>
                            <br />
                        </div>
                    </center>
                </div>
            </div>
        )
    }
}

export default withRouter(LoginForm)