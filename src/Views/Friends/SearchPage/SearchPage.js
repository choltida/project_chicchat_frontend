import React, { Component } from 'react'
import Header from '../../../Components/header'
import api from '../../../Apis/base'
import MenuBar from '../../../Components/menuBar'
import { withRouter } from 'react-router-dom'

class SearchPage extends Component {
  constructor() {
    super()
    this.state = {
      search: '',
      friends: [],
      friendSearch: null,
      inputSearch: false
    }
  }

  componentWillMount() {
    api.get('/showfriend')
      .then(response => {
        if(response.data.code === "0000") {
        this.setState({
          friends: JSON.parse(response.data.friend_list), friendSearch: null
        })
      }
      else {
        alert(response.data.message)
      }
      })
  }

  viewProfile = (e) => {
    api.get('/friend/' + e.target.value)
      .then((response) => {
        if (response.ok && response.status === 200) {
          if (response.data.code === "0000") {
            sessionStorage.setItem('friend', response.data.results)
            this.props.history.push('/Friend')
          }
          else {
            alert(response.data.message)
          }
        }
      })
  }

  addFriend = (e) => {
    api.put('/addfriend/' + e.target.value)
    .then((response) => {
      if (response.ok && response.status === 200) {
        if (response.data.code === "0000") {
          alert(response.data.message)
          window.location.reload()
        }
        else {
          alert(response.data.message)
        }
      }
    })
  }

  unFriend = (e) => {
    api.put('/unfriend/' + e.target.value)
      .then((response) => {
        if (response.ok && response.status === 200) {
          if (response.data.code === "0000") {
            alert(response.data.message)
            window.location.reload()
          }
          else {
            alert(response.data.message)
          }
        }
      })
  }

  searchFriends = (e) => {
    this.setState({
      search: e.target.value
    })
  }

  searchSubmit = (e) => {
    e.preventDefault()

    api.setHeader('token', sessionStorage.getItem('token'))
    api.post('/search', { search: this.state.search })
      .then((response) => {
        console.log('search', response)
        if (response.ok && response.status === 200) {
          if (response.data.code === "0000") {
            this.setState({
              friendSearch: JSON.parse(response.data.results)
            })
          } else {
            alert(response.data.message)
          }
        }
      })
  }

  render() {
    return (
      <div>
        <MenuBar />
        <Header />
        <br />

        <div className="input-group col-md-4">
          <input type="text" className="form-control" name="search" onChange={this.searchFriends} placeholder="Search Profile.." />
          <div className="input-group-append">
            <button type="button" className="input-group-text" onClick={this.searchSubmit}>Search</button>
          </div>
        </div>

        <br />
        {(this.state.friendSearch === null) ? (
          <div className="container">
            <div className="row">

              {this.state.friends && this.state.friends.map(friend => {
                return (
                  <div className="col-3">
                    <div className="card bg-light col-12">
                      <img className="card" src={'http://localhost:3333/uploads/' + friend.imageFriend} alt={friend.nameFriend} width="220" height="180" />
                      <div className="card-footer bg-light">
                        <div className='row'>
                          <div className="col-6">
                            <button onClick={this.viewProfile} type='button' value={friend.idFriend} className="btn btn-link">{friend.username_friend}</button>
                          </div>
                          <div className="col-4">
                            <button onClick={this.unFriend} type='button' value={friend.idFriend} className="btn btn-secondary">Unfriend</button>
                          </div>
                        </div>
                      </div>
                    </div><br />
                  </div>
                )
              })
              }
            </div>
          </div>
        ) : (
            <div className="container">
              <div className="row">

                {this.state.friendSearch && this.state.friendSearch.map(friendsearch => {
                  return (
                    <div className="col-3">
                      <div className="card bg-light col-12">
                        <img className="card" src={'http://localhost:3333/uploads/' + friendsearch.imageFriend} alt={friendsearch.nameFriend} width="220" height="180" />

                        {friendsearch.status === '1' ? (
                          <div className="card-footer bg-light">
                            <div className='row'>
                              <div className="col-6">
                                <button onClick={this.viewProfile} type='button' value={friendsearch.idFriend} className="btn btn-link">{friendsearch.username_friend}</button>
                              </div>
                              <div className="col-4">
                                <button onClick={this.unFriend} type='button' value={friendsearch.idFriend} className="btn btn-secondary">Unfriend</button>
                              </div>
                            </div>
                          </div>) : (
                            <div className="card-footer bg-light">
                              <div className='row'>
                                <div className="col-6">
                                  <button onClick={this.viewProfile} type='button' value={friendsearch.idFriend} className="btn btn-link">{friendsearch.username_friend}</button>
                                </div>
                                <div className="col-4">
                                  <button onClick={this.addFriend} type='button' value={friendsearch.idFriend} className="btn btn-secondary">Addfriend</button>
                                </div>
                              </div>
                            </div>
                          )
                        }
                      </div><br />
                    </div>
                  )
                })
                }

              </div>
            </div>
          )
        }

        {/* <div className="container">
          <div className="row">

            {this.state.friends && this.state.friends.map(friend => {
              return (
                <div className="col-3">
                  <div className="card bg-light col-12">
                    <img className="card" src={'http://localhost:3333/uploads/' + friend.imageFriend} alt={friend.nameFriend} width="220" height="180" />
                    <div className="card-footer bg-light">
                      <div className='row'>
                        <div className="col-6">
                          <button onClick={this.viewProfile} type='button' value={friend.idFriend} className="btn btn-link">{friend.username_friend}</button>
                        </div>
                        <div className="col-4">
                          <button onClick={this.unFriend} type='button' value={friend.idFriend} className="btn btn-secondary">Unfriend</button>
                        </div>
                      </div>
                    </div>
                  </div><br />
                </div>
              )
            })
            }

          </div>
          </div> */}
      </div>
    )
  }
}

export default withRouter(SearchPage)
