import React, { Component, Fragment } from 'react'
import MenuBar from '../../../Components/menuBar'
import Header from '../../../Components/header'
import api from '../../../Apis/base';

class FriendDetail extends Component {
  constructor() {
    super()
    this.state = {
      idFriend: '',
      firstname_friend: '',
      lastname_friend: '',
      username_friend: '',
      email_friend: '',
      mobile_friend: '',
      aboutFriend: '',
      imageFriend: '',
      status: ''
    }
  }

  componentWillMount() {
    var response = JSON.parse(sessionStorage.getItem('friend'))
    if (response !== undefined && response !== null) {
      this.setState({
        idFriend: response[0].idFriend,
        firstname_friend: response[0].firstname_friend,
        lastname_friend: response[0].lastname_friend,
        username_friend: response[0].username_friend,
        email_friend: response[0].email_friend,
        mobile_friend: response[0].mobile_friend,
        aboutFriend: response[0].aboutFriend,
        imageFriend: response[0].imageFriend,
        status: response[0].status
      })
    } else {
      this.setState({
        idFriend: '',
        firstname_friend: '',
        lastname_friend: '',
        username_friend: '',
        email_friend: '',
        mobile_friend: '',
        aboutFriend: '',
        imageFriend: '',
        status: ''
      })
    }
  }

  addFriend = () => {
    api.put('/addfriend/' + this.state.idFriend)
    .then((response) => {
      if (response.ok && response.status === 200) {
        if (response.data.code === "0000") {
          this.props.history.push('/Search')
        }
        else {
          console.log(response.data.code)
        }
      }
    })
  }

  unFriend = () => {
    api.put('/unfriend/' + this.state.idFriend)
      .then((response) => {
        if (response.ok && response.status === 200) {
          if (response.data.code === "0000") {
            alert(response.data.message)
            this.props.history.push('/Search')
          }
          else {
            console.log(response.data.code)
          }
        }
      })
  }

  render() {
    return (
      <Fragment>
        <MenuBar />
        <Header />
        <br />
        {(this.state.status === '0') ? (
          <div className="container">
            <div className='row'>
              <div className='col-3'>
                <img className="rounded border" src={'http://localhost:3333/uploads/' + this.state.imageFriend} alt='..' width="220px" />
              </div>
              <div className='col-6'>
                <h4> {this.state.username_friend} </h4>
                <p> Name: {this.state.firstname_friend} {this.state.lastname_friend}</p>
                <p> About: {this.state.aboutFriend} </p>
                <br />
              </div>
            </div>
            <div className='row'>
              <div className='col-4' />
              <div className='col-7'>
                <button className="btn btn-dark" onClick={this.addFriend}>
                  Add Friend
                </button>
              </div>
            </div>
          </div>
        ) : (
            <div className="container">
              <div className='row'>
                <div className='col-3'>
                  <img className="rounded border" src={'http://localhost:3333/uploads/' + this.state.imageFriend} alt='..' width="220px" />
                </div>
                <div className='col-6'>
                  <h4> {this.state.username_friend} </h4>
                  <p> Name: {this.state.firstname_friend} {this.state.lastname_friend}</p>
                  <p> Email: {this.state.email_friend} </p>
                  <p> Mobile: {this.state.mobile_friend} </p>
                  <p> About: {this.state.aboutFriend} </p>
                  <br />
                </div>
              </div>

              <div className='row'>
                <div className='col-4' />
                <div className='col-7'>
                  <button className="btn btn-dark" onClick={this.unFriend}>
                    UnFriend
                      </button>
                </div>
              </div>
            </div>
          )
        }
      </Fragment>
    )
  }
}

export default FriendDetail