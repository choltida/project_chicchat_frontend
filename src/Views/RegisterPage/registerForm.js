import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { withRouter } from 'react-router-dom'
import api from '../../Apis/base'
import Menu from '../../Components/menuBar'

class RegisterForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            firstname: '',
            lastname: '',
            username: '',
            password: '',
            email: '',
            mobile: '',
            citizen: '',
            profileImage: null,
            showfile: null,
            nameImage:''
        }
    }

    handleChange = e => {
        const { value, name } = e.target
        this.setState({
            [name]: value
        })
    }

    registerSubmit = () => {
        const formData = new FormData()
        formData.append('profileImage', this.state.profileImage)
        formData.append('firstname', this.state.firstname)
        formData.append('lastname', this.state.lastname)
        formData.append('username', this.state.username)
        formData.append('password', this.state.password)
        formData.append('email', this.state.email)
        formData.append('mobile', this.state.mobile)
        formData.append('citizen', this.state.citizen)

        api.post('/register', formData)
            .then((response) => {
                if (response.ok && response.status === 200) {
                    if (response.data.code === "0000") {
                        alert(response.data.message)
                        this.props.history.push('/')
                    }
                    else{
                        alert(response.data.message)
                    }
                }
            }).catch((error) => {
                alert(error)
            })
    }

    onChange = e => {
        this.setState({
            profileImage: e.target.files[0],
            showfile: URL.createObjectURL(e.target.files[0])
        })
    }

    resetFile = e => {
        e.preventDefault();
        this.setState({ showfile: null })
    }

    render() {
        return (
            <div>
                <Menu />
                    <div className='container'>
                        <br />
                        <h2 className="text-left">Register..</h2>
                        <hr />
                        <div className="form-row">
                            <div className="form-group col-md-6">
                                <label>Firstname</label>
                                <input type="text" name="firstname" className="form-control" onChange={this.handleChange} />
                            </div>
                            <div className="form-group col-md-6">
                                <label>Lastname</label>
                                <input type="text" name="lastname" className="form-control" onChange={this.handleChange} />
                            </div>
                        </div>

                        <div className="form-row">
                            <div className="form-group col-md-6">
                                <label>Username</label>
                                <input type="text" name="username" className="form-control" placeholder="" onChange={this.handleChange} />
                            </div>
                            <div className="form-group col-md-6">
                                <label>Password</label>
                                <input type="password" name="password" className="form-control" placeholder="" onChange={this.handleChange} />
                            </div>
                        </div>

                        <div className="form-row">
                            <div className="form-group col-md-6">
                                <label>Email</label>
                                <input type="email" name="email" className="form-control" placeholder="" onChange={this.handleChange} />
                            </div>
                            <div className="form-group col-md-6">
                                <label>Mobile</label>
                                <input type="text" name="mobile" className="form-control" placeholder="" onChange={this.handleChange} />
                            </div>
                        </div>

                        <div className="form-row">
                            <div className="form-group col-md-6">
                                <label>Citizen</label>
                                <input type="text" name="citizen" className="form-control" placeholder="" onChange={this.handleChange} />
                            </div>
                            <div className="form-group col-md-6">
                                <label>Profile Image</label>
                                <div className="custom-file">
                                    <input type="file" className="custom-file-input" name="profileImage" onChange={this.onChange} encType='multipart/form-data'/>
                                    <label className="custom-file-label">Choose file...</label>
                                </div>
                            </div>
                        </div>
                        <div>
                            {this.state.profileImage && (
                                <div style={{ textAlign: "center" }}>
                                    <button className="btn btn-light" onClick={this.resetFile}>Remove File</button>
                                </div>
                            )}
                            <div className="text-center">
                                <img style={{ width: "100", height:"100"}} src={this.state.showfile} alt=''/>
                            </div>
                        </div>
                        <div className="text-center">
                            <button type="submit" value="upload" className="btn btn-secondary" onClick={this.registerSubmit}>Submit</button>
                            &nbsp;
                        <Link to='/' className="btn btn-secondary">Back</Link>
                        </div>
                    </div>
            </div>
        )
    }
}

export default withRouter(RegisterForm)