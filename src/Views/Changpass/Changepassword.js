import React, { Component, Fragment } from 'react'
import MenuBar from '../../Components/menuBar'
import api from '../../Apis/base'

class Changepassword extends Component {
    constructor() {
        super()
        this.state = {
            idUser: '',
            oldPassword: '',
            newPassword: '',
            confirmPassword:'',
            Confirm:false
        }
    }

    componentWillMount() {
        var response = JSON.parse(sessionStorage.getItem('response'))
        if (response !== undefined && response !== null) {
            this.setState({
                idUser: response[0].idUser
            })
        } else {
            this.setState({
                idUser: ''
            })
        }
    }

    handleChange = e => {
        const { value, name } = e.target
        this.setState({
            [name]: value
        })
    }

    changePassword = e => {
        if(this.state.newPassword === this.state.confirmPassword){
            api.post('/changpassword', this.state)
                .then((response) => {
                if (response.ok && response.status === 200) {
                    if (response.data.code === "0000") {
                        alert(response.data.message)
                        sessionStorage.removeItem('isLogin')
                        sessionStorage.removeItem('response')
                        this.props.history.push('/')
                    }
                    else{
                        alert(response.data.message)
                    }
                }
            })
        }else{
            alert('Please confirm the password again.')
        }
    }

    

    render() {
        return (
            <Fragment>
                <MenuBar />
                <div className="container">
                    <br />
                    <h2 className="text-left">Change password..</h2>
                    <hr />
                    <center>
                        <div className="card col-5">
                            <div className="text-left form-row">
                                <div className="form-group col-md-12">
                                    <label className="col-form-label-lg">Old password :</label>
                                    <div className="input-group flex-nowrap">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text"><i className="fas fa-lock-open"/></span>
                                        </div>
                                        <input type="password" name="oldPassword" className="form-control" onChange={this.handleChange} required />
                                    </div>
                                </div>
                            </div>

                            <div className="text-left form-row">
                                <div className="form-group col-md-12">
                                    <label className="col-form-label-lg">New password :</label>
                                    <div className="input-group flex-nowrap">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text"><i className="fas fa-lock"/></span>
                                        </div>
                                        <input type="password" name="newPassword" className="form-control" onChange={this.handleChange} required />
                                    </div>
                                </div>
                            </div>

                            <div className="text-left form-row">
                                <div className="form-group col-md-12">
                                    <label className="col-form-label-lg">Confirm password :</label>
                                    <div className="input-group flex-nowrap">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text"><i className="far fa-check-circle"/></span>
                                        </div>
                                        <input type="password" name="confirmPassword" className="form-control" onChange={this.handleChange} required />
                                    </div>
                                </div>
                            </div>

                                <div className="text-center">
                                    <button className="btn btn-light" onClick={this.changePassword}> Submit </button>
                                </div>
                                <br />
                            </div>
                            <br/>
                    </center>
                </div>
            </Fragment >
                )
            }
        }
        
export default Changepassword