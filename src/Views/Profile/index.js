import React, { Component, Fragment } from 'react'
import MenuBar from '../../Components/menuBar'
import Header from '../../Components/header'
import api from '../../Apis/base'
import { Link } from 'react-router-dom'

class ProfileDetail extends Component {
    constructor() {
        super()
        this.state = {
            idUser: '',
            firstname: '',
            lastname: '',
            username: '',
            password: '',
            email: '',
            mobile: '',
            citizen: '',
            about: '',
            profileImage: null,
            newprofileImage: null,
            showfile: null,
            editprofile: false,
            editProfileimage: false
        }
    }

    componentWillMount() {
        var response = JSON.parse(sessionStorage.getItem('response'))
        if (response !== undefined && response !== null) {
            this.setState({
                idUser: response[0].idUser,
                firstname: response[0].firstname,
                lastname: response[0].lastname,
                username: response[0].username,
                email: response[0].email,
                mobile: response[0].mobile,
                citizen: response[0].citizen,
                about: response[0].about,
                profileImage: response[0].profileImage
            })
        }
    }

    handleChange = e => {
        const { value, name } = e.target
        this.setState({
            [name]: value
        })
    }

    handleSubmit = (e) => {
        api.put('/editprofile/' + e.target.value, this.state)
            .then((response) => {
                if (response.ok && response.status === 200) {
                    console.log(response)
                    if (response.data.code === "0000") {
                        sessionStorage.setItem('response', response.data.results)
                        alert(response.data.message)
                        window.location.reload()
                    }
                    else {
                        alert(response.data.message)
                    }
                }
            })
    }

    editProfile = (e) => {
        this.setState({
            editprofile: true
        })
    }

    editProfileimage = (e) => {
        this.setState({
            editProfileimage: true
        })
    }

    onChange = e => {
        this.setState({
            newprofileImage: e.target.files[0],
            showfile: URL.createObjectURL(e.target.files[0])
        })
    }

    handleProfileimage = e => {
        const formData = new FormData()
        formData.append('newprofileImage', this.state.newprofileImage)
        formData.append('idUser', this.state.idUser)

        api.put('/editprofileimage/' + this.state.idUser, formData)
        .then((response) => {
            if (response.ok && response.status === 200) {
                if (response.data.code === "0000") {
                    sessionStorage.setItem('response', response.data.results)
                    alert(response.data.message)
                    window.location.reload()
                }
                else{
                    alert(response.data.message)
                }
            }
        })
    }

    render() {
        return (
            <Fragment>
                <MenuBar />
                <Header />
                <br />
                <div className="container">
                    <div className='row'>
                        <div className='col-3'>
                            <div align='center'>
                                <img className="rounded" src={'http://localhost:3333/uploads/' + this.state.profileImage} alt='..' width="220px" />
                                <div className="col-md-12">
                                    {this.state.editProfileimage ? (
                                        <div>
                                            <br />
                                            <div className="custom-file">
                                                <input type="file" className="custom-file-input" name="newprofileImage" onChange={this.onChange} encType='multipart/form-data' />
                                                <label className="custom-file-label">{this.state.profileImage}</label>
                                            </div>

                                            <div>
                                                {this.state.showfile && (
                                                    <div style={{ textAlign: "center" }}>
                                                        <button type='button' className="btn btn-link" onClick={this.handleProfileimage}>Used as a Profileimage</button>
                                                    </div>
                                                )}
                                                <div className="text-center">
                                                    <img className="rounded" style={{ width: "100%" }} src={this.state.showfile} alt='' />
                                                </div>
                                            </div>
                                        </div>) :
                                        (<div>
                                            <Link to={'/Changepassword'}>Change password</Link>
                                            <button type='button' className="btn btn-link" onClick={this.editProfileimage}>Change Profileimage</button>
                                        </div>)}
                                </div>
                            </div>
                        </div>
                        <div className='col-9'>
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label>Firstname</label>
                                    <input disabled={!this.state.editprofile} type="text" name="firstname" placeholder={this.state.firstname} className="form-control" onChange={this.handleChange} />
                                </div>
                                <div className="form-group col-md-6">
                                    <label>Lastname</label>
                                    <input disabled={!this.state.editprofile} type="text" name="lastname" placeholder={this.state.lastname} className="form-control" onChange={this.handleChange} />
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label>Username</label>
                                    <input disabled={!this.state.editprofile} type="text" name="username" className="form-control" placeholder={this.state.username} onChange={this.handleChange} />
                                </div>
                                <div className="form-group col-md-6">
                                    <label>Email</label>
                                    <input disabled={!this.state.editprofile} type="email" name="email" className="form-control" placeholder={this.state.email} onChange={this.handleChange} />
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label>Mobile</label>
                                    <input disabled={!this.state.editprofile} type="text" name="mobile" className="form-control" placeholder={this.state.mobile} onChange={this.handleChange} />
                                </div>
                                <div className="form-group col-md-6">
                                    <label>Citizen</label>
                                    <input disabled={!this.state.editprofile} type="text" name="citizen" className="form-control" placeholder={this.state.citizen} onChange={this.handleChange} />
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="col-12">
                                    <label>About</label>
                                    <textarea disabled={!this.state.editprofile} type="text" name="about" className="form-control" placeholder={this.state.about} rows='3' onChange={this.handleChange} />
                                </div>
                            </div>
                            <br />
                            {this.state.editprofile ? (<div align='center' >
                                <button className="btn btn-dark" type='button' value={this.state.idUser} onClick={this.handleSubmit}>Submit</button>
                            </div>) : (<div align='center' >
                                <button className="btn btn-dark" type='button' value={this.state.idUser} onClick={this.editProfile}>Edit</button>
                            </div>)}
                        </div>
                    </div>
                </div>
            </Fragment >
        )
    }
}

export default ProfileDetail