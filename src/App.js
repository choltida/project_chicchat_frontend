import React, { Component } from 'react'
import './App.css'

import Routing from './Routers'
// import MenuBar from './Components/menuBar';
 
class App extends Component {
  render() {
    return (
      <div>
        {/* <MenuBar /> */}
        <Routing />
      </div>
    )
  }
}

export default App
