import { create } from 'apisauce'

const api = create({
  baseURL: 'http://localhost:3333',
})


api.addMonitor(response => {
  
  if(response.data.hasOwnProperty('token'))
  {
    api.setHeader('token', response.data.token)
    sessionStorage.setItem('token',response.data.token)
  }
})

api.addRequestTransform(request => {
  if(request.headers['token'] === undefined && request.url !== '/login') {
    //console.log('save token')
    //console.log(localStorage.getItem('token'))
    // api.setHeader('token', localStorage.getItem('token'))
    request.headers['token'] = sessionStorage.getItem('token')
  }

})




export default api