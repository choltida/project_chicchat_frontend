import React from 'react'
import { Route, Switch } from 'react-router-dom'

import LoginPage from './Views/LoginPage'
import RegisterPage from './Views/RegisterPage'
import SearchPage from './Views/Friends/SearchPage/SearchPage'
import FriendDetail from './Views/Friends/FriendDetail'
import Profile from './Views/Profile'
import Changepassword from './Views/Changpass/Changepassword'


export default () => (
  <Switch>
    <Route exact path="/" component={LoginPage} />
    <Route exact path="/Register" component={RegisterPage} />
    <Route exact path="/Search" component={SearchPage} />
    <Route exact path="/Friend" component={FriendDetail} />
    <Route exact path="/Profile" component={Profile} />
    <Route exact path="/Changepassword" component={Changepassword} />
  </Switch>
)