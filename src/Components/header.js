import React, { Component } from 'react'

class Header extends Component {
    constructor() {
        super()
        this.state = {
            firstname: '',
            lastname: ''
        }
    }

        componentWillMount(){
            var response = JSON.parse(sessionStorage.getItem('response'))
            if (response !== undefined && response !== null) {
                this.setState({
                    firstname: response[0].firstname,
                    lastname: response[0].lastname,
                })
            } else {
                this.setState({
                    firstname: '',
                    lastname: ''
                })
            }
        }

    render() {
        return (
            <div>
                <div className="card bg-light">
                    <div className="card-body">
                        <div className="container">
                            <br />
                            <br />
                            <br />
                            <h1 className="card-title">Chic-Chat</h1>
                            <div className='row'>
                                <div className='col-1' />
                                <div className='col-11'>
                            <h5>Hello : {this.state.firstname} {this.state.lastname}</h5>
                            </div>
                            </div>
                            <br />
                            <br />
                            <br />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Header