import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { withRouter } from 'react-router-dom'

class MenuBar extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLogin: false,
      firstname: '',
      lastname: '',
      profileImage: ''
    }
  }

  heandleLogout = () => {
    sessionStorage.removeItem('isLogin')
    sessionStorage.removeItem('response')
    this.props.history.push('/')
  }

  componentWillMount() {
    var response = JSON.parse(sessionStorage.getItem('response'))
    if (response !== undefined && response !== null) {
      this.setState({
        isLogin: sessionStorage.getItem('isLogin'),
        firstname: response[0].firstname,
        lastname: response[0].lastname,
        profileImage: response[0].profileImage,
      })
    } else {
      this.setState({
        isLogin: false,
        firstname: '',
        lastname: '',
        profileImage: ''
      })
    }

  }

  render() {
    return (
      <div>
        {this.state.isLogin ? (
          <nav className="navbar navbar-dark bg-dark">
            <div className="navbar-brand">
            <Link to="/Search" className="text-white"><i className="fas fa-home fa-lg"/></Link>
            </div>
            <div className="navbar-end">
              <Link to="/Profile" className="text-white">
                <span className="icon">
                  <img className="rounded-circle" style={{ width: "35px" , height: "35px" }} src={'http://localhost:3333/uploads/'+this.state.profileImage} alt='' />
                </span>
                <span> {this.state.firstname} {this.state.lastname} </span>
              </Link>
              &nbsp;
                      <button to="/" className="btn btn-light" onClick={this.heandleLogout}>
                <span className="icon">
                  <i className="fas fa-sign-out-alt" />
                </span>
                <span> Logout </span>
              </button>
            </div>
          </nav>
        ) : (<nav className="navbar navbar-dark bg-dark">
          <div className="navbar-brand">
            <span />
            <span />
            <span />
          </div>
          <div className="navbar-end">
            <Link to="/Register" className="btn btn-light">
              <span className="icon">
                <i className="fas fa-pen" />
              </span>
              <span> Register </span>
            </Link>
          </div>
        </nav>
          )
        }
      </div>
    )
  }
}
export default withRouter(MenuBar)